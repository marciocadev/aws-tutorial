from aws_cdk import (
    RemovalPolicy,
    Stack,
    aws_dynamodb as _dynamo,
    aws_lambda as _lambda,
    aws_lambda_destinations as _lmb_dest,
    aws_events as _events,
    aws_events_targets as _events_targets,
    aws_sns as _sns,
    aws_sns_subscriptions as _sns_subs,
    aws_iam as _iam,
    aws_apigateway as _gtw
)
from constructs import Construct

class EventBridgeStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        bus = _events.EventBus(
            scope=self, 
            id='company-bus', 
            event_bus_name='company-bus')

        topic = _sns.Topic(
            scope=self,
            id="async-msg",
            display_name="async-msg",
            topic_name="async-msg"
        )

        company_db = _dynamo.Table(
            scope=self,
            id="company-db",
            table_name="company-db",
            partition_key=_dynamo.Attribute(
                name="identity",
                type=_dynamo.AttributeType.STRING
            ),
            billing_mode=_dynamo.BillingMode.PAY_PER_REQUEST,
            removal_policy=RemovalPolicy.DESTROY
        )

        include_address_lmb = _lambda.Function(
            scope=self,
            id="include-address",
            function_name="include-address",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="include_address.handler",
            code=_lambda.Code.from_asset("lambda_fns/include_address"),
            environment={
                "TABLE_NAME": company_db.table_name
            }
        )
        company_db.grant_write_data(grantee=include_address_lmb)
        include_address_rule = _events.Rule(
            scope=self, 
            id="include-address-rule", 
            rule_name="include-address-rule",
            event_bus=bus
        )
        include_address_rule.add_event_pattern(
            detail={
                "requestContext": { "condition": ["Success"] },
                "responsePayload": {
                    "source": ["employee", "company"],
                    "action": ["include-address"]
                }
            }
        )
        include_address_rule.add_target(
            target=_events_targets.LambdaFunction(handler=include_address_lmb)
        )

        company_registration_lmb = _lambda.Function(
            scope=self,
            id="company-registration",
            function_name="company-registration",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="company_registration.handler",
            code=_lambda.Code.from_asset("lambda_fns/company_registration"),
            environment={
                "TABLE_NAME": company_db.table_name
            },
            on_success=_lmb_dest.EventBridgeDestination(event_bus=bus)
        )
        company_db.grant_write_data(grantee=company_registration_lmb)
        topic.add_subscription(_sns_subs.LambdaSubscription(fn=company_registration_lmb))

        register_employee_lmb = _lambda.Function(
            scope=self,
            id="register-employee",
            function_name="register-employee",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="register_employee.handler",
            code=_lambda.Code.from_asset("lambda_fns/register_employee"),
            environment={
                "TABLE_NAME": company_db.table_name
            },
            on_success=_lmb_dest.EventBridgeDestination(event_bus=bus)
        )
        company_db.grant_write_data(grantee=register_employee_lmb)
        _events.Rule(
            scope=self,
            id="register-employee-rule",
            rule_name="register-employee-rule",
            event_bus=bus,
            event_pattern=_events.EventPattern(
                source=['receive-payload'],
                detail_type=['message'],
                detail={ "status": ["success"] }
            ),
            targets=[_events_targets.LambdaFunction(handler=register_employee_lmb)]
        )

        receive_payload_lmb = _lambda.Function(
            scope=self,
            id="receive-payload",
            function_name="receive-payload",
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="receive_payload.handler",
            code=_lambda.Code.from_asset("lambda_fns/receive_payload"),
            environment={
                "EVENT_BRIDGE_BUS_NAME": bus.event_bus_name,
                "TOPIC_ARN": topic.topic_arn
            }
        )
        topic.grant_publish(grantee=receive_payload_lmb)
        receive_payload_lmb.grant_invoke(grantee=_iam.ServicePrincipal("apigateway.amazonaws.com"))
        event_policy = _iam.PolicyStatement(
            effect=_iam.Effect.ALLOW,
            resources=['*'],
            actions=['events:PutEvents'])
        receive_payload_lmb.add_to_role_policy(event_policy)

        gtw = _gtw.RestApi(
            scope=self,
            id="gtw-event-bridge",
            rest_api_name="gtw-event-bridge"
        )
        root = gtw.root
        resource = root.add_resource(path_part="company")
        resource.add_method(
            http_method="POST",
            integration=_gtw.LambdaIntegration(
                handler=receive_payload_lmb
            )
        )
