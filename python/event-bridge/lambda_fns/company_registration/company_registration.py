import json
import boto3

def handler(event, context):
    print(event)

    company = json.loads(event["Records"][0]["Sns"]["Message"])

    
    return {
        "source": "company",
        "action": "include-address",
        "address": company
    }
