import os
import json
import boto3

event_bridge_client = boto3.client("events")
sns_resource = boto3.resource("sns")

def handler(event, context):

    body = json.loads(event["body"])
    employees = body["employees"]
    company = body["company"]

    event_bus = os.environ.get("EVENT_BRIDGE_BUS_NAME")
    topic_arn = os.environ.get("TOPIC_ARN")
    
    topic = sns_resource.Topic(topic_arn)
    topic.publish(Message=json.dumps(company))

    for employee in employees:
        resp = event_bridge_client \
            .put_events(
                Entries=[
                    {
                        "EventBusName": event_bus,
                        "Source": "receive-payload",
                        "DetailType": "message",
                        "Detail": json.dumps(
                            {
                                "status": "success",
                                "employee": employee
                            }
                        )
                    }
                ]
            )
        print(resp)
