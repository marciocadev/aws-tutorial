import json
import boto3

def handler(event, context):

    detail = event["detail"]
    employee = detail["employee"]
    
    return {
        "source": "employee",
        "action": "include-address",
        "address": employee
    }

