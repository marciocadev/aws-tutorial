# vscode-debug

Este projeto demonstra como executar o debug do vscode em um projeto cdk em python

será criado um lambda e será inserido um dado em uma tabela dynamo para demostrar a integração entre componentes

## Rodando teste unitário
export AWS_DEFAULT_REGION=us-east-1
python -m pytest
python -m pytest --html=report.html

# Debugando um projeto da AWS no VSCode 

Em um projeto CDK existem 2 debugs possiveis, o debug da criação da stack e o debug dos lambdas

Abaixo demonstro o necessário para executar cada debug

## Debug da Stack

Para debug a stack é necessário criar uma configuração específica para a stack conforme abaixo, lembrando que executar este debug não gera uma stack e ela basicamente gera os templates para criação da stack (CDK Synth)

```
{
    "configurations": [
        {
            "name": "CDK Synth",
            "type": "python",
            "request": "launch",
            "program": "app.py",
            "console": "integratedTerminal"
        }
    ]
}
```

* cdk synth --no-staging > template.yml
* sam local invoke gatewaylambdadynamopostpy81C7B684 --no-event
* sam local invoke gatewaylambdadynamopostpy81C7B684 --env-vars env.json -e event.json --profile marcio-dev

* https://www.sentiatechblog.com/debugging-a-cdk-python-project-in-visual-studio-code

* https://www.sentiatechblog.com/when-your-lambdas-are-living-on-the-edge-lambda-edge

# Welcome to your CDK Python project!

This is a blank project for Python development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

Enjoy!
