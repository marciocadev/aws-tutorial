import json
import boto3

def get_resources():
    session = boto3.Session()
    result = []
    for resource in session.get_available_resources():
        result.append(resource)
    return result

def handler(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps(get_resources())
    }