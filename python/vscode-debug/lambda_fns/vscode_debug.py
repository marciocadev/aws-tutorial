import json
import os
import boto3
import uuid

db = boto3.resource("dynamodb")

def handler(event, context):

    body = json.loads(event.get("body"))
    title = body.get("title")

    if title is None or not title.strip():
        return {
            "statusCode": 422,
            "body": "Invalid data"
        }

    table_name = os.environ.get("TABLE_NAME")
    table = db.Table(table_name)

    table.put_item(
        Item={
            "id": uuid.uuid4().hex,
            "title": title
        }
    )

    return {
        "statusCode": 200,
        "body": "Item inserido com sucesso"
    }