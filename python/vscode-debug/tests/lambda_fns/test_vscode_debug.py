import unittest
from unittest.mock import Mock
from lambda_fns import vscode_debug


class VscodeDebugTest(unittest.TestCase):

    @classmethod 
    def setup_class(cls):
        mock_table = Mock()
        vscode_debug.db = Mock()
        vscode_debug.db.Table = Mock()
        vscode_debug.db.Table.return_value = mock_table
        mock_table.put_item = Mock()
        mock_table.put_item.return_value = {"success": "true"}

    @classmethod 
    def teardown_class(cls):
        print("ok")

    def test_vscode_debug_success(self):
        self.assertEqual(
            {
                "statusCode": 200, 
                "body": "Item inserido com sucesso"
            }, 
            vscode_debug.handler(
                {
                    "body": '{"title": "3d"}'
                },
                {}
            )
        )

    def test_vscode_debug_fail_title_empty(self):
        self.assertEqual(
            {
                "statusCode": 422,
                "body": "Invalid data"
            }, 
            vscode_debug.handler(
                {
                    "body": '{"title": ""}'
                },
                {}
            )
        )

    def test_vscode_debug_fail_title_missing(self):
        self.assertEqual(
            {
                "statusCode": 422,
                "body": "Invalid data"
            }, 
            vscode_debug.handler(
                {
                    "body": '{"aaa": "aaa"}'
                },
                {}
            )
        )

