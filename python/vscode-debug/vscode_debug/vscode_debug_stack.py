from aws_cdk import (
    RemovalPolicy,
    Stack,
    aws_iam as _iam,
    aws_lambda as _lambda,
    aws_dynamodb as _dynamo,
    aws_apigateway as _gtw
)
from constructs import Construct

class VscodeDebugStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        table = self.create_table(table_name="vscode-debug-dy-py")

        post_lmb = self.create_post_lmb(
            lmb_name="vscode-debug-lmb-py",
            env_table_name=table.table_name
        )
        list_resources_lmb = self.create_list_resources(
            lmb_name="list_resources"
        )
        list_services_lmb = self.create_list_services(
            lmb_name="list_services"
        )

        gateway = self.create_rest_gateway(rest_api_name="gtw-vscode-debug")

        post_lmb.grant_invoke(grantee=_iam.ServicePrincipal("apigateway.amazonaws.com"))
        table.grant_write_data(post_lmb)

        list_resources_lmb.grant_invoke(grantee=_iam.ServicePrincipal("apigateway.amazonaws.com"))
        list_services_lmb.grant_invoke(grantee=_iam.ServicePrincipal("apigateway.amazonaws.com"))
        
        root = gateway.root

        resource = root.add_resource(path_part="books")
        resource.add_method(http_method="POST",
                            integration=_gtw.LambdaIntegration(handler=post_lmb))

        lists = root.add_resource(path_part="list")
        resources = lists.add_resource(path_part="resources")
        resources.add_method(http_method="GET", 
                            integration=_gtw.LambdaIntegration(handler=list_resources_lmb))
        resources = lists.add_resource(path_part="services")
        resources.add_method(http_method="GET", 
                            integration=_gtw.LambdaIntegration(handler=list_services_lmb))


    def create_rest_gateway(self, rest_api_name: str) -> _gtw.RestApi:
        gtw = _gtw.RestApi(
            scope=self,
            id="gtw-vscode-debug",
            rest_api_name=rest_api_name
        )
        return gtw

    def create_list_services(self, lmb_name: str):
        ''' Get list of services '''
        lmb = _lambda.Function(
            scope=self,
            id="list-services-lmb-py",
            function_name=lmb_name,
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="list_services.handler",
            code=_lambda.Code.from_asset("lambda_fns")
        )
        return lmb

    def create_list_resources(self, lmb_name: str):
        ''' Get list of resources '''
        lmb = _lambda.Function(
            scope=self,
            id="list-resources-lmb-py",
            function_name=lmb_name,
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="list_resources.handler",
            code=_lambda.Code.from_asset("lambda_fns")
        )
        return lmb

    def create_post_lmb(self, 
                lmb_name: str,
                env_table_name: str) -> _lambda.Function:
        ''' Create lambda for persist data on dynamo '''
        lmb = _lambda.Function(
            scope=self,
            id="vscode-debug-lmb-py",
            function_name=lmb_name,
            runtime=_lambda.Runtime.PYTHON_3_8,
            handler="vscode_debug.handler",
            code=_lambda.Code.from_asset("lambda_fns"),
            environment={
                "TABLE_NAME": env_table_name
            }
        )
        return lmb

    def create_table(self, table_name: str) -> _dynamo.Table:
        ''' Create dynamodb table '''
        table = _dynamo.Table(
            scope=self,
            id="vscode-debug-dy-py",
            table_name=table_name,
            partition_key=_dynamo.Attribute(
                name="id",
                type=_dynamo.AttributeType.STRING
            ),
            billing_mode=_dynamo.BillingMode.PAY_PER_REQUEST,
            removal_policy=RemovalPolicy.DESTROY
        )
        return table