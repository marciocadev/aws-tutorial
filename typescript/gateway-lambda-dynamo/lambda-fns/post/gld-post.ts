const bcrypt = require("bcrypt");

const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB();

export const handler = async (event: any) => {

    const {body} = event;
    const {title} = body;
    
    if (title === undefined) {
        return {
            "statusCode": 422,
            "body": "Invalid data"
        }
    }

    let hash:string = await bcrypt.hash(title, 10);

    const params = {
        TableName: process.env.TABLE_NAME,
        Item: {
            'id': {S: hash},
            'title': {S: title}
        }
    }
    
    let result = await dynamo.putItem(params).promise();


    return {
        "statusCode": 200,
        "body": "Success"
    }
}

export default handler;