import { 
  Stack, 
  StackProps,
  aws_dynamodb as _dynamo,
  aws_lambda as _lambda,
  aws_iam as _iam,
  aws_logs as _logs,
  RemovalPolicy
} from 'aws-cdk-lib';
import { Construct } from 'constructs';

export class GatewayLambdaDynamoStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const table = new _dynamo.Table(
      this,
      "gateway_lambda_dynamo_book_ts",
      {
        tableName: "gateway_lambda_dynamo_book_ts",
        partitionKey: {
          name: "id",
          type: _dynamo.AttributeType.STRING
        },
        billingMode: _dynamo.BillingMode.PAY_PER_REQUEST,
        removalPolicy: RemovalPolicy.DESTROY
      }
    )

    const postLmb = new _lambda.Function(
      this,
      "gateway_lambda_dynamo_post_ts",
      {
        functionName: "gateway_lambda_dynamo_post_ts",
        runtime: _lambda.Runtime.NODEJS_14_X,
        handler: "gld-post.handle",
        code: _lambda.Code.fromAsset("lambda-fns/post"),
        environment: {
          "TABLE_NAME": table.tableName
        }
      }
    )

    table.grantWriteData(postLmb)

    postLmb.grantInvoke(
      new _iam.ServicePrincipal("apigateway.amazonaws.com")
    )

    const logs = new _logs.LogGroup(
      this,
      "LogConfig",
      {
        logGroupName: '/aws/lambda/' + postLmb.functionName,
        retention: _logs.RetentionDays.ONE_DAY,
        removalPolicy: RemovalPolicy.DESTROY
      }
    )
  }
}
