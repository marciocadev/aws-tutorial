import {handler} from '../../lambda-fns/post/gld-post';

describe("teste", function() {
    it("Success", async() => {
        const result = await handler(
            {
                body: {
                    title: "3d"
                }
            });
        expect(result.statusCode).toEqual(200);
        expect(result.body).toEqual('Success');
    })

    it("Empty title", async() => {
        const result = await handler(
            {
                body: {}
            });
        expect(result.statusCode).toEqual(422);
        expect(result.body).toEqual('Invalid data');
    })
});